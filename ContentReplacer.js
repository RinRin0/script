// ==UserScript==
// @name        Content Replacer
// @namespace   Violentmonkey Scripts
// @updateURL       https://gitlab.com/RinRin0/script/-/raw/master/ContentReplacer.js
// @downloadURL     https://gitlab.com/RinRin0/script/-/raw/master/ContentReplacer.js
// @include     http://www.syiban.com/*
// @grant       none
// @version     1.0.3
// @author      Hana
// ==/UserScript==
document.body.innerHTML = document.body.innerHTML.replace(/A、/g, ' ');
document.body.innerHTML = document.body.innerHTML.replace(/B、/g, ' ');
document.body.innerHTML = document.body.innerHTML.replace(/C、/g, ' ');
document.body.innerHTML = document.body.innerHTML.replace(/D、/g, ' ');
document.body.innerHTML = document.body.innerHTML.replace(/A./g, ' ');
document.body.innerHTML = document.body.innerHTML.replace(/B./g, ' ');
document.body.innerHTML = document.body.innerHTML.replace(/C./g, ' ');
document.body.innerHTML = document.body.innerHTML.replace(/D./g, ' ');
